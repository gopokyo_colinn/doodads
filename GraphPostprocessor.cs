using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Text;
using System;

using ParadoxNotion.Serialization;

class JsonHelper
{
    private const string INDENT_STRING = "    ";
    private const string LINE_INDENT_STRING = "    ";
    public static string FormatJson(string str)
    {
        var indent = 0;
        var quoted = false;
        var sb = new StringBuilder();
        for (var i = 0; i < str.Length; i++)
        {
            var ch = str[i];
            switch (ch)
            {
                case '{':
                case '[':
                    sb.Append(ch);
                    if (!quoted)
                    {
                        sb.AppendLine(); sb.Append(LINE_INDENT_STRING);
                        Enumerable.Range(0, ++indent).ToList().ForEach(item => sb.Append(INDENT_STRING));
                    }
                    break;
                case '}':
                case ']':
                    if (!quoted)
                    {
                        sb.AppendLine(); sb.Append(LINE_INDENT_STRING);
                        Enumerable.Range(0, --indent).ToList().ForEach(item => sb.Append(INDENT_STRING));
                    }
                    sb.Append(ch);
                    break;
                case '"':
                    sb.Append(ch);
                    bool escaped = false;
                    var index = i;
                    while (index > 0 && str[--index] == '\\')
                        escaped = !escaped;
                    if (!escaped)
                        quoted = !quoted;
                    break;
                case ',':
                    sb.Append(ch);
                    if (!quoted)
                    {
                        sb.AppendLine(); sb.Append(LINE_INDENT_STRING);
                        Enumerable.Range(0, indent).ToList().ForEach(item => sb.Append(INDENT_STRING));
                    }
                    break;
                case ':':
                    sb.Append(ch);
                    if (!quoted)
                        sb.Append(" ");
                    break;
                default:
                    sb.Append(ch);
                    break;
            }
        }
        return sb.ToString();
    }
}

class GraphPostprocessor : AssetPostprocessor
{

    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        if (JSONSerializer.applicationPlaying)
        {
            return;
        }

        foreach (string str in importedAssets)
        {
            if (str.StartsWith("Assets/Canvai") && str.EndsWith(".asset"))
            {
                string[] lines = File.ReadAllLines(str);
                List<string> newlines = new List<string>();
                const string graphLine = "  _serializedGraph: '";
                if (lines.Any(l => l.StartsWith(graphLine)))
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        var line = lines[i];
                        if (line.StartsWith(graphLine))
                        {
                            while (lines[i + 1].StartsWith("    "))
                            {
                                line += lines[i + 1].Substring(3);  // 3 to pick up an extra space to try and hack around a strang bug
                                i++;
                            }

                            newlines.Add("  _serializedGraph: >");
                            newlines.Add("    " + JsonHelper.FormatJson(line.Substring(graphLine.Length, line.Length - graphLine.Length - 1)));
                        }
                        else
                        {
                            newlines.Add(line);
                        }
                    }

                    File.WriteAllLines(str + ".bak", lines);
                    File.WriteAllLines(str, newlines);
                }
            }
        }
    }
}


